% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.R
\docType{data}
\name{example_references}
\alias{example_references}
\title{References of an element in a diagram of a test MINERVA instance}
\format{
A list with the following variables:
\itemize{
\item \emph{link}: a link to a given reference in the source database
\item \emph{type}: a type of the resource (internal to the MINERVA Platform)
\item \emph{resource}: a stable identifier of a given resource
\item \emph{id}: internal MINERVA identifier of this reference
\item \emph{annotatorClassName}: if annotated externally - by which annotator
}
}
\usage{
example_references
}
\description{
References of an element in a diagram of a test MINERVA instance
}
\keyword{datasets}
