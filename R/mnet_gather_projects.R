## mnet_gather_projects.R

#' @title Retrieve project information across MINERVA-Net machines
#'
#' @description Function connecting to MINERVA-Net repository, it collects information about available projects
#' filtering by information on disesase and organism.
#'
#' @param disease (`character`) MeSH id of a disease to filter by, can be a character vector
#' @param organism (`character`) Taxonomy ID of an organism to filter by, can be a character vector
#' @param with_creation_dates (`boolean`) if TRUE, for all returned projects it will request their upload dates
#' @param config (`httr::config`) a 'httr::config' object overriding the default config for this call
#'
#' @return (`tbl`) a data frame with all projects matching the query
#' @examples
#'
#' mnet_gather_projects(disease = "D010300", with_creation_dates = TRUE)
#'
#' @export
mnet_gather_projects <- function(disease = NULL, organism = NULL,
                                 with_creation_dates = FALSE, config = httr::config()) {
  this_page <- 0
  resp <- jsonlite::fromJSON(ask_GET(paste0("https://minerva-net.lcsb.uni.lu/api/machines/?sort=id&page=",this_page,"&size=50"),
                                     config = config))
  mnet_ids <- with(resp$pageContent, id[status == "OK"])
  while(!resp$isLastPage) {
    this_page <- this_page + 1
    resp <- jsonlite::fromJSON(ask_GET(paste0("https://minerva-net.lcsb.uni.lu/api/machines/?sort=rootUrl&page=",this_page,"&size=50"),
                                       config = config))
    mnet_ids <- c(mnet_ids, with(resp$pageContent, id[status == "OK"]))
  }
  all_projects <- do.call(rbind, lapply(mnet_ids, mnet_get_projects, disease, organism))
  if(with_creation_dates) {
    dates <- apply(all_projects, 1,
                   function(x) jsonlite::fromJSON(ask_GET(paste0(x["machine.rootUrl"],
                                                                 "api/projects/",
                                                                 x["projectId"]),
                                                          config = config))$creationDate)
    all_projects <- data.frame(all_projects, creation_date = dates)

  }
  return(all_projects)
}

