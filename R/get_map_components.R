# get_map_components.R

#' @title Get `map_components` for a given MINERVA API url.
#'
#' @description Function that gets the components of a given map/project on the MINERVA Platform:
#' - `models` summarise available diagrams
#' - `elements` contain information about diagram elements
#' - `reactions` contain information about diagram reactions.
#' Number of rows in `models` corresponds to the number of `data.frame` objects
#' in `elements` and `reactions` lists.
#'
#' @param map_api (`character`) The address of the MINERVA Platform to query
#' @param project_id (`character`) the id of the project to query; if NULL the default project will be queried
#' @param token (`character`) for private projects, a token returned by the `minervar::login()` function
#' @param get_elements (`logical`) if TRUE elements will be retrieved
#' @param get_reactions (`logical`) if TRUE reactions will be retrieved
#' @param e_columns (`character`) a concatenated list of element parameters
#' @param r_columns (`character`) a concatenated list of reaction parameters
#' @param config (`httr::config`) a 'httr::config' object overriding the default config for this call
#'
#' @return (`list`) a list containing 1) information about all the diagrams in the proejct, 2) elements and 3) reactions in the project
#'
#' @examples
#'
#' get_map_components("https://minerva-service.lcsb.uni.lu/minerva/api/")
#' get_map_components("https://minerva-service.lcsb.uni.lu/minerva/api/",
#'                    project_id = "minervar_example",
#'                    get_reactions = FALSE,
#'                    e_columns = "id,name,type,bounds")
#'
#' @export
get_map_components <- function(map_api, project_id = NULL, token = NULL,
                               get_elements = TRUE,
                               get_reactions = TRUE,
                               e_columns = "",
                               r_columns = "",
                               config = httr::config()) {
  if(!endsWith(map_api, "/minerva/api/")) {
    warning("minervar:get_map_components(): Incorrect 'map_api'. It should end with '/minerva/api/'")
    return(NULL)
  }
  if(is.null(project_id)) {
    ### If project id not given, get configuration of the map, to obtain the latest (default) version
    project_id <- get_default_project(map_api)
  }
  ### The address of the latest (default) build
  mnv_base <- paste0(map_api, "projects/",project_id,"/")

  message("minervar:get_map_components(): Asking for diagrams in: ", mnv_base, "models/")

  ### Get diagrams
  models <- jsonlite::fromJSON(ask_GET(paste0(mnv_base, "models/"), token = token, config = config))

  map_components <- list(models = models)

  if(e_columns != "") { e_columns <- paste0("?columns=", e_columns) }
  if(r_columns != "") { r_columns <- paste0("?columns=", r_columns) }

  if(get_elements) {
    ### Get elements of the chosen diagram
    model_elements <- lapply(models$idObject,
                             function(x)
                               jsonlite::fromJSON(ask_GET(paste0(mnv_base,"models/",x,"/","bioEntities/elements/",e_columns),
                                                          token = token, config = config),
                                                  flatten = F))
    names(model_elements) <- models$idObject
    map_components[["map_elements"]] <- model_elements
  }

  if(get_reactions) {
    ### Request for reactions the chosen diagram
    model_reactions <- lapply(models$idObject,
                              function(x)
                                jsonlite::fromJSON(ask_GET(paste0(mnv_base,"models/",x,"/","bioEntities/reactions/",r_columns),
                                                           token = token, config = config),
                                                   flatten = F))
    names(model_reactions) <- models$idObject
    map_components[["map_reactions"]] <- model_reactions
  }

  ### Return the list that was populated across the function above
  return(map_components)
}
