# xml_celldesigner_add_enclosing_container.R

#' @title Add an enclosing compartment to the CellDesigner diagram
#'
#' @description For a given CellDesigner diagram as a path to an `xml2` file,
#' create a compartment enclosing all its elements, and assign a text of choice to its notes
#'
#' @param cd_diagram (`character`) a CellDesigner diagram as a string to be read by the `xml2::read_xml()` function, or a path to zn xml file
#' @param name (`character`) name of the enclosing compartment
#' @param notes (`character`) notes of the enclosing compartment
#' @param margin (`numeric`) distance of the compartment from the outer diagram border
#' @param color (`character`) color of the enclosing compartment, hexadecimal format
#'
#' @return (`character`) a CellDesigner diagram enclosed in a compartment
#' @examples
#'
#' xml_celldesigner_add_enclosing_container(example_celldesigner_pathway,
#'                                          "example container", "exmple notes")
#'
#' @export
xml_celldesigner_add_enclosing_container <- function(cd_diagram, name, notes = "", margin = 10, color = "FF6667AB") {
  ### Check correctness of 'cd_diagram'
  ### Read first and see if this gives an xml2 object
  diag <- xml2::read_xml(cd_diagram)
  if(!all(class(diag) == c("xml_document", "xml_node")))  {
    warning("minervar:xml_compact_celldesigner_ids(): Not an 'xml2' object")
    return(NULL)
  }

  ### Define the namespace object with XML namespaces for CellDesignerfor later xml2::find queries
  celldesigner_ns <-
    xml2::xml_ns(xml2::read_xml("<root>
                                  <sbml xmlns:sbml = \"http://www.sbml.org/sbml/level2/version4\"/>
                                  <cd xmlns:cd = \"http://www.sbml.org/2001/ns/celldesigner\"/>
                                </root>"))

  ### Add a compartment

  ### Update listOfCompartments (name and notes)
  compartments <- xml2::xml_find_all(diag, "//sbml:compartment", ns = celldesigner_ns)
  cids <- purrr::map_chr(compartments, xml2::xml_attr, "id") %>% sort()
  ### If only one available (default) use "c0"
  if(length(cids) == 1) {
    newcid <-"c0"
  } else {
    newcid <- paste0(utils::tail(cids,1), "_container")
  }


  xml2::xml_add_sibling(compartments[[1]],
                        ### xml2::read_xml raises warnings when introducing an undefined namespace
                        ### defining it explicitly here may cause problems to CellDesigner format, as globally it's defined implicitly
                        suppressWarnings(xml2::read_xml(
                          paste0("<compartment metaid='", newcid, "' id='", newcid, "' name='", name, "' size='1' units='volume' outside='default'>",
                                 "<notes><html xmlns='http://www.w3.org/1999/xhtml'>",
                                 "<head><title/></head>",
                                 "<body>", notes, "</body>",
                                 "</html></notes>",
                                 "<annotation><celldesigner:extension>",
                                 "<celldesigner:name>", name, "</celldesigner:name>",
                                 "</celldesigner:extension></annotation></compartment>"))))

  ### Update compartments pointing to "default"
  purrr::walk(xml2::xml_find_all(diag, "//sbml:compartment[@outside='default']", ns = celldesigner_ns),
                      ~ xml2::xml_set_attr(., "outside", newcid))

  ### Update species pointing to "default"
  purrr::walk(xml2::xml_find_all(diag, "//sbml:species[@compartment='default']", ns = celldesigner_ns),
                      ~ xml2::xml_set_attr(., "compartment", newcid))

  ### Update celldesigner:listOfCompartmentAliases (new celldesigner:compartmentAlias)
  compartment_aliases <- xml2::xml_find_all(diag, "//cd:compartmentAlias", ns = celldesigner_ns)
  newcaid <- "ca0"
  caids <- purrr::map_chr(compartment_aliases, xml2::xml_attr, "id") %>% sort()
  if(length(caids) > 0) { newcaid <- paste0(utils::tail(caids,1), "_container") }

  ### Get model dimensions
  diag_dims <- xml2::xml_find_first(diag, "//cd:modelDisplay", ns = celldesigner_ns) %>%
    xml2::xml_attrs() %>% as.numeric()

  xml2::xml_add_child(xml2::xml_find_first(diag, "//cd:listOfCompartmentAliases", ns = celldesigner_ns),
                      ### xml2::read_xml raises warnings when introducing an undefined namespace
                      ### defining it explicitly here may cause problems to CellDesigner format, as globally it's defined implicitly
                      suppressWarnings(xml2::read_xml(
                          paste0("<celldesigner:compartmentAlias id='", newcaid, "' compartment='", newcid,"'>",
                                 "<celldesigner:class>SQUARE</celldesigner:class>",
                                 "<celldesigner:bounds x='", margin, "' y='", margin, "' ",
                                 "w='", (diag_dims[1] - 2*margin), "' h='", (diag_dims[2] - 2*margin), "'/>",
                                 "<celldesigner:namePoint x='", 2*margin, "' y='", 2*margin, "'/>",
                                 "<celldesigner:doubleLine thickness='12.0' outerWidth='2.0' innerWidth='1.0'/>",
                                 "<celldesigner:paint color='", color, "' scheme='Color'/>",
                                 "</celldesigner:compartmentAlias>"))))

  ### Update species aliases without compartment alias or complexSpeciesAlias ("default")
  purrr::walk(xml2::xml_find_all(diag, "//cd:speciesAlias[not(@complexSpeciesAlias) and not(@compartmentAlias)]",
                                 ns = celldesigner_ns),
              ~ xml2::xml_set_attr(., "compartmentAlias", newcaid))
  ### Update compartment species aliases without compartment alias ("default")
  purrr::walk(xml2::xml_find_all(diag, "//cd:complexSpeciesAlias[not(@compartmentAlias)]",
                                 ns = celldesigner_ns),
              ~ xml2::xml_set_attr(., "compartmentAlias", newcaid))
  return(as.character(diag))
}

test_xml_celldesigner_add_enclosing_container <- function(cd_diagram, name, notes = "", margin = 10, color = "FF6667AB") {
  ### Check correctness of 'cd_diagram'
  ### Read first and see if this gives an xml2 object
  diag <- xml2::read_xml(cd_diagram)
  if(!all(class(diag) == c("xml_document", "xml_node")))  {
    warning("minervar:xml_compact_celldesigner_ids(): Not an 'xml2' object")
    return(NULL)
  }

  ### Define the namespace object with XML namespaces for CellDesignerfor later xml2::find queries
  celldesigner_ns <-
    xml2::xml_ns(xml2::read_xml("<root>
                                  <sbml xmlns:sbml = \"http://www.sbml.org/sbml/level2/version4\"/>
                                  <cd xmlns:cd = \"http://www.sbml.org/2001/ns/celldesigner\"/>
                                </root>"))

  ### Add a compartment

  ### Update listOfCompartments (name and notes)
  compartments <- xml2::xml_find_all(diag, "//sbml:compartment", ns = celldesigner_ns)
  cids <- purrr::map_chr(compartments, xml2::xml_attr, "id") %>% sort()
  newcid <- paste0(utils::tail(cids,1), "_container")

  xml2::xml_add_sibling(compartments[[1]],
                        ### xml2::read_xml raises warnings when introducing an undefined namespace
                        ### defining it explicitly here may cause problems to CellDesigner format, as globally it's defined implicitly
                        suppressWarnings(xml2::read_xml(
                          paste0("<compartment metaid='", newcid, "' id='", newcid, "' name='", name, "' size='1' units='volume' outside='default'>",
                                 "<notes><html xmlns='http://www.w3.org/1999/xhtml'>",
                                 "<head><title/></head>",
                                 "<body>", notes, "</body>",
                                 "</html></notes>",
                                 "<annotation><celldesigner:extension>",
                                 "<celldesigner:name>", name, "</celldesigner:name>",
                                 "</celldesigner:extension></annotation></compartment>"))))

  ### Update compartments pointing to "default"
  purrr::walk(xml2::xml_find_all(diag, "//sbml:compartment[@outside='default']", ns = celldesigner_ns),
              ~ xml2::xml_set_attr(., "outside", newcid))

  ### Update species pointing to "default"
  purrr::walk(xml2::xml_find_all(diag, "//sbml:species[@compartment='default']", ns = celldesigner_ns),
              ~ xml2::xml_set_attr(., "compartment", newcid))

  ### Update celldesigner:listOfCompartmentAliases (new celldesigner:compartmentAlias)
  compartment_aliases <- xml2::xml_find_all(diag, "//cd:compartmentAlias", ns = celldesigner_ns)
  newcaid <- "default_container"
  caids <- purrr::map_chr(compartment_aliases, xml2::xml_attr, "id") %>% sort()
  if(length(caids) > 0) { newcaid <- paste0(utils::tail(caids,1), "_container") }

  ### Get model dimensions
  diag_dims <- xml2::xml_find_first(diag, "//cd:modelDisplay", ns = celldesigner_ns) %>%
    xml2::xml_attrs() %>% as.numeric()

  xml2::xml_add_child(xml2::xml_find_first(diag, "//cd:listOfCompartmentAliases", ns = celldesigner_ns),
                      ### xml2::read_xml raises warnings when introducing an undefined namespace
                      ### defining it explicitly here may cause problems to CellDesigner format, as globally it's defined implicitly
                      suppressWarnings(xml2::read_xml(
                        paste0("<celldesigner:compartmentAlias id='", newcaid, "' compartment='", newcid,"'>",
                               "<celldesigner:class>SQUARE</celldesigner:class>",
                               "<celldesigner:bounds x='", margin, "' y='", margin, "' ",
                               "w='", (diag_dims[1] - 2*margin), "' h='", (diag_dims[2] - 2*margin), "'/>",
                               "<celldesigner:namePoint x='", 2*margin, "' y='", 2*margin, "'/>",
                               "<celldesigner:doubleLine thickness='12.0' outerWidth='2.0' innerWidth='1.0'/>",
                               "<celldesigner:paint color='", color, "' scheme='Color'/>",
                               "</celldesigner:compartmentAlias>"))))

  ### Update species aliases without compartment alias or complexSpeciesAlias ("default")
  purrr::walk(xml2::xml_find_all(diag, "//cd:speciesAlias[not(@complexSpeciesAlias) and not(@compartmentAlias)]",
                                 ns = celldesigner_ns),
              ~ xml2::xml_set_attr(., "compartmentAlias", newcid))
  ### Update compartment species aliases without compartment alias ("default")
  purrr::walk(xml2::xml_find_all(diag, "//cd:complexSpeciesAlias[not(@compartmentAlias)]",
                                 ns = celldesigner_ns),
              ~ xml2::xml_set_attr(., "compartmentAlias", newcid))
  return(as.character(diag))
}

